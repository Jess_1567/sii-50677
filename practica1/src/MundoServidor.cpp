// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>

#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

#define TAM_BUFF 55
#define TAM_CAD 200

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
bool acabar=false;

void* hilo_comandos1(void* d);
void* hilo_comandos2(void* d);
void* accept_clients(void* d);

//Manejadores y gestion de senales
void tratar_senales(int signum){
	if (signum == SIGKILL){
        	printf("Recibida SIGKILL!\n");
		exit(1);
    	}
	if (signum == SIGINT){
        	printf("Recibida SIGINT!\n");
		exit(1);
    	}
	if (signum == SIGTERM){
        	printf("Recibida SIGTERM!\n");
		exit(1);
    	}
	if (signum == SIGPIPE){
        	printf("Recibida SIGPIPE!\n");
		exit(1);
    	}
	if (signum == SIGUSR1){
        	printf("Received SIGUSR1!\n");
		exit(0);
    	}
}

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{	
<<<<<<< HEAD
	//Destruccion de la tuberia del logger
	close(fd);
	
	//Finalizacion de los sockets de conexion y de comunicacion
	mysocket_conexion.Close();
	mysocket_comunicacion.Close();
=======
	//Finalizacion correcta de los hilos
	acabar=true;
	pthread_join(thid1,NULL);
	pthread_join(thid2,NULL);
	pthread_join(thid3,NULL);
	
	//Finalizacion de la tuberia
	close(fd);

	//Finalizacion de los sockets correctamente
	for (int i=conexiones.size()-1; i>=0; i--)
		conexiones[i].Close();
	servidor.Close();
>>>>>>> practica5
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	int i;
	char buff[TAM_BUFF];
	char cad_send[TAM_CAD];
	

	esfera.Mueve(0.025f);
	esfera.radio-=0.0005;

	//Regulacion del tamano de la pelota
	if(esfera.radio<0.2f) esfera.radio=0.2f;
	if(esfera.radio-0.2f<0.001f && esfera.radio-0.2f>-0.001f) esfera.radio=0.5f;

	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=3+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=3+2*rand()/(float)RAND_MAX;
		puntos2++;

		sprintf(buff,"El jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);
	}


	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-3-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-3-2*rand()/(float)RAND_MAX;
		puntos1++;

		sprintf(buff,"El jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos1);
	}

	//Escritura en la tuberia para el logger
	write(fd,buff,sizeof(buff));

	//Sockets
	sprintf(cad_send,"%f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.x2, jugador1.y1, jugador1.y2, jugador2.x1, jugador2.x2, jugador2.y1, jugador2.y2, puntos1, puntos2);

<<<<<<< HEAD
	mysocket_comunicacion.Send(cad_send,sizeof(cad_send));//Envio de las teclas 
=======
	for (i=conexiones.size()-1; i>=0; i--) {
         	if (conexiones[i].Send(cad_send,sizeof(cad_send)) <= 0) {
            	conexiones.erase(conexiones.begin()+i);
            		if (i < 2) {// Hay menos de dos clientes conectados
              			// Se resetean los puntos a cero
				puntos1=0;
				puntos2=0;
         		}
		}
        }
>>>>>>> practica5
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-3;break;
//	case 'w':jugador1.velocidad.y=3;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	
	
	//Apertura de la tuberia
	fd=open("/tmp/mififo1",O_WRONLY);

	//Tratamiento de señales
	struct sigaction act;
	act.sa_handler=tratar_senales;
	act.sa_flags=0;
	sigemptyset(&act.sa_mask);
	sigaction(SIGINT,&act,NULL);
	sigaction(SIGTERM,&act,NULL);
	sigaction(SIGPIPE,&act,NULL);
	sigaction(SIGUSR1,&act,NULL);

	//Sockets
	char ip[]="127.0.0.1";
	int puerto=3550;

	servidor.InitServer((char*)ip,puerto);

	//Creacion de los hilos
	pthread_create(&thid3, NULL, accept_clients, this);
	pthread_create(&thid1, NULL, hilo_comandos1, this);
	pthread_create(&thid2, NULL, hilo_comandos2, this);
}

//Implementacion de la funcionalidad de los hilos
void* accept_clients(void* d)
{
      CMundo* p=(CMundo*) d;
      p->GestionaConexiones();
}

void* hilo_comandos1(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador1();
}

void* hilo_comandos2(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador2();
}
void CMundo::RecibeComandosJugador1()
{
     while (!acabar) {
	    if(conexiones.size()>0){
            usleep(10);
            char cad[TAM_CAD];
            conexiones[0].Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
	    }
      }
}

void CMundo::RecibeComandosJugador2()
{
     while (!acabar) {
	    if(conexiones.size()>1){
            usleep(10);
            char cad[TAM_CAD];
            conexiones[1].Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
	    }
      }
}

void CMundo::GestionaConexiones(){
	while (!acabar) {
			Socket client;
			client=servidor.Accept();
			conexiones.push_back(client);
	}
}
